//
//  FavoriteDetailViewController.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 18.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit
import Social

class FavoriteDetailViewController: UIViewController {

    var quote: QuoteRealm?
    var interactor: Interactor?
    
    override func loadView() {
        let contentView = FavoriteDetailView(frame: .zero)
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let favoriteDetailView = view as? FavoriteDetailView else { fatalError() }
        favoriteDetailView.nameLabel.text = quote?.name
        favoriteDetailView.quoteLabel.text = quote?.body
        favoriteDetailView.shareButton.addTarget(self, action: #selector(shareQuote), for: .touchUpInside)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        favoriteDetailView.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let userDefaults = UserDefaults.standard
        if !userDefaults.bool(forKey: "TutorialShown") {
            showTutorialScreen()
            userDefaults.set(true, forKey: "TutorialShown")
        }
        
    }
    
    func showTutorialScreen() {
        let alertController = UIAlertController(title: NSLocalizedString("Hinweis", comment: "Hinweis"), message: "Nach unten ziehen zum Schließen", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    
    func handlePan(inSender: UIPanGestureRecognizer) {
        
        let percentThreshold:CGFloat = 0.3
        
        // convert y-position to downward pull progress (percentage)
        let translation = inSender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        
        guard let interactor = interactor else { return }
        
        switch inSender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }

    func shareQuote(sender: AnyObject?) {

        guard let quote = quote else { return }
        
        let sheet = UIAlertController(title: nil, message: "Teilen mit", preferredStyle: .actionSheet)
        
        let twitterAction = UIAlertAction(title: "Twitter", style: .default) { (action) in
            if let theComposer = self.composerFor(SLServiceTypeTwitter, quote: quote) {
                self.present(theComposer, animated: true, completion: nil)
            }
            else {
                self.warningFor("Twitter")
            }
        }
                
        let faceBookAction = UIAlertAction(title: "Facebook", style: .default) { (action) in
            if let theComposer = self.composerFor(SLServiceTypeFacebook, quote: quote) {
                self.present(theComposer, animated: true, completion: nil)
            }
            else {
                self.warningFor("Facebook")
            }
        }
        
        sheet.addAction(twitterAction)
        sheet.addAction(faceBookAction)
        sheet.addAction(UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil))
        present(sheet, animated: true, completion: nil)
    }
    
    
    private func composerFor(_ service: String, quote: QuoteRealm) -> SLComposeViewController? {
        let available = SLComposeViewController.isAvailable(forServiceType: service)
        if !available {
            return nil
        }
        
        let composer = SLComposeViewController(forServiceType: service)
        composer?.setInitialText("\"\(quote.body)\" - \(quote.name)")
        return composer
    }
    
    func warningFor(_ inService: String) {
        let alertController = UIAlertController(title: "\(inService) nicht verfügbar", message: "\(inService)-Konto nicht gefunden", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
}
