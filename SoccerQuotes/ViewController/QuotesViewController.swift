//
//  ViewController.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 10/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit
import Social
import RealmSwift

class QuotesViewController: UIViewController, AddFavorite {
    
    // MARK: - Properties
    
    private var collectionView: UICollectionView?
    var currentPage = 0
    var totalPages = 0
    var totalQuotes = 0
    let cellTag = 1337
    var blurEffectView: UIVisualEffectView?
    
    var quotes = [Quote]()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        automaticallyAdjustsScrollViewInsets = false
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        guard let collectionView = collectionView else { fatalError() }
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = UIColor(red: 0.153, green: 0.682, blue: 0.376, alpha: 0.70)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.register(QuoteCollectionViewCell.self, forCellWithReuseIdentifier: "QuoteCell")
        collectionView.register(LoadingCollectionViewCell.self, forCellWithReuseIdentifier: "LoadingCell")
        
        view.addSubview(collectionView)
        let views = ["collectionView": collectionView]
        
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "|[collectionView]|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[collectionView]", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        NSLayoutConstraint.activate(constraints)
        
        NotificationCenter.default.addObserver(self, selector: #selector(fetchQuotesAfterDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchQuotes()
    }
    
    func fetchQuotesAfterDidBecomeActive(inNotification: NSNotification) {
        fetchQuotes()
    }
    
    func fetchQuotes() {

        APICLient.fetchQuotes(currentPage: currentPage) { [weak self] (inQuotes, inTotalPages, inTotalQuotes, inError)  in
            
            if let _ = inError {
                DispatchQueue.main.async {
                    if let cell = self?.collectionView?.cellForItem(at: IndexPath(item: 0, section: 0)) as? LoadingCollectionViewCell {
                        cell.activityIndicatorView.stopAnimating()
                    }
                    self?.showWarningFromAPI(inMessage: "Da ging etwas schief, bitte später erneut versuchen")
                }
            }
            else {
                
                for aQuote in inQuotes {
                    guard let theQuotes = self?.quotes else { return }
                    if !theQuotes.contains(where: { $0 == aQuote }) {
                        self?.quotes.append(aQuote)
                    }
                }
                
                self?.totalPages = inTotalPages
                self?.totalQuotes = inTotalQuotes
            }
            DispatchQueue.main.async {
                self?.collectionView?.reloadData()
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let collectionView = collectionView else { fatalError() }
        NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
    }
    
    
    // MARK: - Custom Actions
    func shareQuote(sender: AnyObject?) {
        guard let buttonPosition = sender?.convert(CGPoint.zero, to: collectionView) else { fatalError() }
        guard let indexPath = collectionView?.indexPathForItem(at: buttonPosition) else { fatalError() }
        let quote = quotes[indexPath.item]
        
        let sheet = UIAlertController(title: nil, message: "Teilen mit", preferredStyle: .actionSheet)
        
        let twitterAction = UIAlertAction(title: "Twitter", style: .default) { (action) in
            if let theComposer = self.composerForService(service: SLServiceTypeTwitter, quote: quote) {
                self.present(theComposer, animated: true, completion: nil)
            }
            else {
                self.warningForService(inService: "Twitter")
            }
        }
        
        let faceBookAction = UIAlertAction(title: "Facebook", style: .default) { (action) in
            if let theComposer = self.composerForService(service: SLServiceTypeFacebook, quote: quote) {
                self.present(theComposer, animated: true, completion: nil)
            }
            else {
                self.warningForService(inService: "Facebook")
            }
        }

        sheet.addAction(twitterAction)
        sheet.addAction(faceBookAction)
        sheet.addAction(UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil))
        present(sheet, animated: true, completion: nil)
    }
    
    func quoteIsFavorite(inQuote: Quote) -> Bool {
        let realm = try! Realm()
        let quotes = realm.objects(QuoteRealm.self).filter("id = %d", inQuote.id)
        return quotes.count > 0
    }
    
    private func composerForService(service: String, quote: Quote) -> SLComposeViewController? {
        let available = SLComposeViewController.isAvailable(forServiceType: service)
        if !available {
            return nil
        }
        
        let composer = SLComposeViewController(forServiceType: service)
        composer?.setInitialText("\"\(quote.body)\" - \(quote.name)")
        return composer
    }
    
    func warningForService(inService: String) {
        let alertController = UIAlertController(title: "\(inService) nicht verfügbar", message: "\(inService)-Konto nicht gefunden", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func showWarningFromAPI(inMessage: String) {
        
        if self.presentedViewController is UIAlertController {
            return
        }
        else {
            let alertController = UIAlertController(title: "Warning", message: inMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func switchToLatestItem() {
        guard let theCollectionView = collectionView else { return }
        
        let item  = self.collectionView(theCollectionView, numberOfItemsInSection: 0) - 1
        let lastItemIndexPath = IndexPath(item: item, section: 0)
        theCollectionView.scrollToItem(at: lastItemIndexPath, at: .right, animated: true)
    }
    
    func addToFavorites(sender: AnyObject?) {
        guard let buttonPosition = sender?.convert(CGPoint.zero, to: collectionView) else { fatalError() }
        guard let indexPath = collectionView?.indexPathForItem(at: buttonPosition) else { fatalError() }
        let quote = quotes[indexPath.item]

        if quoteIsFavorite(inQuote: quote) {
            let alertController = UIAlertController(title: "Hinweis", message: "Zitat befindet sich bereits in Ihren Favoriten.\nJetzt anzeigen?", preferredStyle: .alert)
            let showAction = UIAlertAction(title: "Ja", style: .default, handler: { (action) in
                guard let tabBarController = self.parent?.tabBarController, let navigationController = tabBarController.viewControllers?[2] as? UINavigationController, let favoritesTableViewController = navigationController.topViewController as? FavoritesTableViewController else { return }
                tabBarController.selectedViewController = tabBarController.viewControllers![2]
                favoritesTableViewController.fetchQuoteAndShow(inQuoteId: quote.id)
            })
            
            let cancelAction = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
            alertController.addAction(showAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        }
        else {
            let realmQuote = QuoteRealm(value: ["id": quote.id, "name": quote.name, "body": quote.body, "createdAt": quote.createdAt])
            let realm = try! Realm()
            try! realm.write {
                realm.add(realmQuote)
            }
            collectionView?.reloadItems(at: [indexPath])
        }
    }
}

