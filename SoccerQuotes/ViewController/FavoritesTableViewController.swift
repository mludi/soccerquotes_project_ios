//
//  FavoritesTableViewController.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 16.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit
import RealmSwift
import QuartzCore

class FavoritesTableViewController: UITableViewController {

    let interactor = Interactor()
    var quotes: Results<QuoteRealm>?
    let realm = try! Realm()
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(FavoriteQuoteTableViewCell.self, forCellReuseIdentifier: "FavoriteQuoteCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 10.0
        tableView.backgroundColor = UIColor(red: 0.404, green: 0.776, blue: 0.561, alpha: 1.00)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        readQuotesAndUpdateUI()
    }

    func readQuotesAndUpdateUI() {
        quotes = realm.objects(QuoteRealm.self)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        
    }
    
    func fetchQuoteAndShow(inQuoteId: Int) {
        let quotes = realm.objects(QuoteRealm.self).filter("id = %d", inQuoteId)
        if quotes.count > 0 {
            guard let theQuote = quotes.first else { return }
            showQuote(inQuote: theQuote)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let quotes = quotes else { return 0 }
        return quotes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteQuoteCell", for: indexPath) as? FavoriteQuoteTableViewCell else { return UITableViewCell() }
        guard let quotes = quotes else { fatalError() }
        let quote = quotes[indexPath.row]
        cell.nameLabel.text = quote.name
        cell.bodyLabel.text = "\u{00AB} \(quote.body.truncate(inLength: 20)) \u{00BB}"
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let quotes = quotes else { fatalError() }
        let quote = quotes[indexPath.row]
        showQuote(inQuote: quote)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let quotes = quotes else { fatalError() }
            let quoteToDelete = quotes[indexPath.row]
            
            try! realm.write {
                realm.delete(quoteToDelete)
                self.readQuotesAndUpdateUI()
            }
        }
    }
    
    func showQuote(inQuote: QuoteRealm) {
        let favoriteDetailViewController = FavoriteDetailViewController()
        favoriteDetailViewController.quote = inQuote
        favoriteDetailViewController.interactor = interactor
        favoriteDetailViewController.transitioningDelegate = self
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.window?.rootViewController?.present(favoriteDetailViewController, animated: true, completion: nil)
    }
    
}

extension FavoritesTableViewController: UIViewControllerTransitioningDelegate {
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
}
