//
//  SubmitQuoteViewController.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 22.05.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class SubmitQuoteViewController: UIViewController, QuoteSubmit, UITextViewDelegate, UITextFieldDelegate {


    weak var activeField: UITextView?
    var heightConstraint: NSLayoutConstraint?
    
    // MARK: - Properties
    
    // MARK: - ViewLifeCycle
    
    override func loadView() {
        let contentView = SubmitQuoteView(frame: .zero)
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        submitQuoteView.textView.delegate = self
        submitQuoteView.nameTextField.delegate = self
        let tapGestureRecoginizer = UITapGestureRecognizer(target: self, action: #selector(done))
        submitQuoteView.scrollView.addGestureRecognizer(tapGestureRecoginizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        
        heightConstraint = NSLayoutConstraint(item: submitQuoteView.scrollView, attribute: .bottom, relatedBy: .equal, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1.0, constant: -20.0)
        heightConstraint?.isActive = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterFromKeyboardNotifications()
        super.viewWillDisappear(animated)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func unregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func keyboardWasShown(inNotification: NSNotification) {
        guard let info = inNotification.userInfo, let submitQuoteView = view as? SubmitQuoteView else { return }
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
        let scrollView = submitQuoteView.scrollView
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets

    }
    
    func keyboardWillBeHidden(inNotification: NSNotification) {
        guard let submitQuoteView = view as? SubmitQuoteView else { return }
        submitQuoteView.scrollView.contentInset = .zero
        submitQuoteView.scrollView.scrollIndicatorInsets = .zero
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        submitQuoteView.textView.resignFirstResponder()

    }
    
    func submitQuote() {
        if canSubmitQuote() {
            print("can submit")
            guard let submitQuoteView = view as? SubmitQuoteView, let theName = submitQuoteView.nameTextField.text, let theBody = submitQuoteView.textView.text else { fatalError() }
            let theTrimmedBody = theBody.trimmingCharacters(in: .whitespacesAndNewlines)
            
            APICLient.submitQuote(theName, body: theTrimmedBody, completionHandler: { [weak self] (error) in
                if let _ = error {
                    DispatchQueue.main.async {
                        self?.showWarningFromAPI(inMessage: "Da ging etwas schief, bitte später erneut versuchen")
                        return
                    }
                }
                self?.success(inQuoteView: submitQuoteView)
            })
        }
        else {
            let alertController = UIAlertController(title: "Hinweis", message: "Bitte alle Felder ausfüllen", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }

    func showWarningFromAPI(inMessage: String) {
        let alertController = UIAlertController(title: "Hinweis", message: inMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    func success(inQuoteView: SubmitQuoteView) {
        let alertController = UIAlertController(title: "Info", message: "Das Zitat wurde zur Prüfung freigegeben", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (inAction) in
            inQuoteView.nameTextField.text = ""
            inQuoteView.textView.text = ""
            self.view.endEditing(true)
            inQuoteView.textView.placeholderLabel.isHidden = false
        }))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func done() {
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        submitQuoteView.textView.endEditing(true)
    }
    
    private func canSubmitQuote() -> Bool {
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        
        return !(submitQuoteView.nameTextField.text?.removeWhitespace().isEmpty)! && !(submitQuoteView.textView.text?.removeWhitespace().isEmpty)!
        
    }
    
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        guard let submitQuoteView = view as? SubmitQuoteView else { fatalError() }
        if !textView.hasText {
            submitQuoteView.textView.placeholderLabel.isHidden = false
        }
        else {
            submitQuoteView.textView.placeholderLabel.isHidden = true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        activeField = textView
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        activeField = nil
        if !textView.hasText {
            if let submitQuoteView = view as? SubmitQuoteView {
                submitQuoteView.textView.placeholderLabel.isHidden = false
            }
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.endEditing(true)
            return false
        }
        return true
    }
    
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


}
