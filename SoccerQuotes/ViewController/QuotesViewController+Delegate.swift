//
//  QuotesViewController+Delegate.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 23/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit


extension QuotesViewController: UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UICollectionViewDelegate {
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if cell.tag == cellTag {
            currentPage += 1
            fetchQuotes()
        }
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        guard let collectionView = collectionView else { fatalError() }
        //        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        //        let visiblePoint = CGPoint(x: CGRectGetMidX(visibleRect), y: CGRectGetMidY(visibleRect))
        //        if let visibleIndexPath = collectionView.indexPathForItemAtPoint(visiblePoint) {
        //            pageControl?.currentPage = visibleIndexPath.row
        //            print(visibleIndexPath.row)
        //        }
    }
    
}
