//
//  QuotesViewController+DataSource.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 23/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

extension QuotesViewController: UICollectionViewDataSource {
    // MARK: - UICollectionViewDatasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if currentPage == 0 {
            return 1
        }
        if currentPage < totalPages {
            return quotes.count + 1
        }
        return quotes.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item < quotes.count {
            return quoteCellForIndexPath(inIndexPath: indexPath, collectionView: collectionView)
        }
        return loadingCellForIndexPath(inIndexPath: indexPath, collectionView: collectionView)
    }
    
    private func quoteCellForIndexPath(inIndexPath: IndexPath, collectionView: UICollectionView) -> QuoteCollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuoteCell", for: inIndexPath) as? QuoteCollectionViewCell else { return QuoteCollectionViewCell() }
        let quote = quotes[inIndexPath.row]
        let theLeft = "\u{00AB}"
        let theRight = "\u{00BB}"

        
        let quoteIsFavorite = self.quoteIsFavorite(inQuote: quote)
        let theColor: UIColor = quoteIsFavorite ? .yellow : .white
        cell.favoriteButton.setFATitleColor(color: theColor)
//        cell.favoriteButton.enabled = !quoteIsFavorite
        
        cell.quoteLabel.text = "\(theLeft) \(quote.body) \(theRight)"
        cell.nameLabel.text = quote.name
        cell.shareButton.addTarget(self, action: #selector(shareQuote), for: .touchUpInside)
        
        return cell
    }
    
    private func loadingCellForIndexPath(inIndexPath: IndexPath, collectionView: UICollectionView) -> LoadingCollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadingCell", for: inIndexPath) as? LoadingCollectionViewCell else { return LoadingCollectionViewCell() }
        cell.activityIndicatorView.startAnimating()
        cell.tag = cellTag
        return cell
        
    }
}
