//
//  SubmitQuoteView.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 22.05.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class SubmitQuoteView: UIView {

    
    let descriptionLabel: UILabel
    let nameTextField: UITextField
    let textView: MLTextView
    let submitButton: UIButton
    let scrollView: UIScrollView
    let toolBar: UIToolbar
    
    
    override init(frame: CGRect) {
        
        descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = "Hier kannst du ein eigenes Zitat übermitteln. Bitte den Autor und das Zitat eintragen"
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = .white
        
        nameTextField = UITextField()
        nameTextField.translatesAutoresizingMaskIntoConstraints = false
        nameTextField.borderStyle = .roundedRect
        nameTextField.placeholder = NSLocalizedString("Autor", comment: "Author")
        nameTextField.returnKeyType = .done

        guard let font = UIFont(name: "HelveticaNeue", size: 16.0) else { fatalError() }
        let attributes = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : font
        ]
        
        nameTextField.attributedPlaceholder = NSAttributedString(string: "Autor", attributes: attributes)
        
        textView = MLTextView(frame: .zero, textContainer: nil)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        textView.layer.cornerRadius = 5.0
        
        submitButton = UIButton(type: .system)
        submitButton.backgroundColor = .white
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.setTitle(NSLocalizedString("Abschicken", comment: ""), for: .normal)
        submitButton.addTarget(nil, action: .submitQuote, for: .touchUpInside)
        submitButton.layer.cornerRadius = 4.0
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: .done)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)

        toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0.404, green: 0.776, blue: 0.561, alpha: 1.00)
        toolBar.sizeToFit()

        
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false

        
//        textView.inputAccessoryView = toolBar
        
        super.init(frame: frame)
        
        addSubview(descriptionLabel)
        addSubview(nameTextField)
        addSubview(textView)
        addSubview(submitButton)
        
        
        addSubview(scrollView)
        
        let contentView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(contentView)
        
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(nameTextField)
        contentView.addSubview(textView)
        contentView.addSubview(submitButton)

        var constraints = [NSLayoutConstraint]()
        
        let views = ["scrollView": scrollView, "contentView": contentView, "textView": textView, "descriptionLabel": descriptionLabel, "submitButton": submitButton, "nameTextField": nameTextField]

        constraints += NSLayoutConstraint.constraints(withVisualFormat: "|-0-[scrollView]-0-|", options: [], metrics: nil, views: views)

        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[scrollView]", options: [], metrics: nil, views: views)
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "|-0-[contentView]-0-|", options: [], metrics: nil, views: views)
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[contentView]-0-|", options: [], metrics: nil, views: views)

        constraints.append(contentView.widthAnchor.constraint(equalTo: self.widthAnchor))

        constraints += NSLayoutConstraint.constraints(withVisualFormat: "|-10-[descriptionLabel]-10-|", options: [], metrics: nil, views: views)
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[descriptionLabel]-20-[nameTextField]-20-[textView]-20-[submitButton]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        
        constraints.append(NSLayoutConstraint(item: textView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0.33, constant: 1.0))
        
        NSLayoutConstraint.activate(constraints)
        
        backgroundColor = UIColor(red: 0.404, green: 0.776, blue: 0.561, alpha: 1.00)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


private extension Selector {
    static let submitQuote = #selector(QuoteSubmit.submitQuote)
    static let done = #selector(QuoteSubmit.done)
}

@objc protocol QuoteSubmit {
    @objc func submitQuote()
    @objc func done()
}
