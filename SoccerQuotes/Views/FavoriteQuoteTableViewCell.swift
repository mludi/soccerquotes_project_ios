//
//  FavoriteQuoteTableViewCell.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 18.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class FavoriteQuoteTableViewCell: UITableViewCell {

    let nameLabel: UILabel
    let bodyLabel: UILabel
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        nameLabel = UILabel()
        nameLabel.textAlignment = .center
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.numberOfLines = 0
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
        
        
        bodyLabel = UILabel()
        bodyLabel.textAlignment = .center
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        bodyLabel.numberOfLines = 0
        bodyLabel.textColor = .white
        
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        backgroundColor = .clear
        contentView.addSubview(nameLabel)
        contentView.addSubview(bodyLabel)
        
        let views = ["nameLabel": nameLabel, "bodyLabel": bodyLabel]
        
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "|-[nameLabel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[nameLabel]-[bodyLabel]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        
        
        NSLayoutConstraint.activate(constraints)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    

}
