//
//  MLTextView.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 05.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class MLTextView: UITextView {


    let placeholderLabel: UILabel
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        
        placeholderLabel = UILabel()
        
        
        placeholderLabel.textColor = .lightGray
        placeholderLabel.font = UIFont(name: "HelveticaNeue", size: 16.0)
        placeholderLabel.textAlignment = .left
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        placeholderLabel.text = NSLocalizedString("Zitat", comment: "")
        
        super.init(frame: frame, textContainer: textContainer)
        
        addSubview(placeholderLabel)
        
        
        var constraints = [NSLayoutConstraint]()

        constraints.append(placeholderLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10.0))
        constraints.append(placeholderLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 10.0))
        constraints.append(NSLayoutConstraint(item: placeholderLabel, attribute: .topMargin, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1.0, constant: 5.0))
        constraints.append(NSLayoutConstraint(item: placeholderLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0))
        
        NSLayoutConstraint.activate(constraints)
        
        self.tintColor = .lightGray
        self.textColor = .black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
