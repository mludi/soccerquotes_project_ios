//
//  LoadingCollectionViewCell.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 18/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class LoadingCollectionViewCell: UICollectionViewCell {
    
    let activityIndicatorView: UIActivityIndicatorView
    
    override init(frame: CGRect) {
        
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        
        super.init(frame: frame)
        
        contentView.addSubview(activityIndicatorView)
        
        var constraints = [NSLayoutConstraint]()
        constraints.append(activityIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        constraints.append(activityIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor))
        NSLayoutConstraint.activate(constraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
