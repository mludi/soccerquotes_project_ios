//
//  QuoteCollectionViewCell.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 11/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

class QuoteCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    let nameLabel: UILabel
    let quoteLabel: UILabel
    let shareButton: UIButton
    let favoriteButton: UIButton

    // MARK: - Init
    
    override init(frame: CGRect) {
        
        nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.numberOfLines = 0
        nameLabel.textColor = .white
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.boldSystemFont(ofSize: 35.0)
        
        
        quoteLabel = UILabel()
        quoteLabel.translatesAutoresizingMaskIntoConstraints = false
        quoteLabel.numberOfLines = 0
        quoteLabel.textColor = .white
        quoteLabel.textAlignment = .center
        quoteLabel.font = UIFont.systemFont(ofSize: 22.0)
        
        shareButton = UIButton(type: .system)
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        shareButton.setFAIcon(icon: .FAShare, iconSize: 40.0, forState: .normal)
        shareButton.setFATitleColor(color: .white, forState: .normal)
        
        favoriteButton = UIButton(type: .system)
        favoriteButton.translatesAutoresizingMaskIntoConstraints = false
        favoriteButton.setFAIcon(icon: .FAStar, iconSize: 40.0, forState: .normal)
        favoriteButton.addTarget(nil, action: .addToFavorites, for: .touchUpInside)
        
        super.init(frame: frame)
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(quoteLabel)
        contentView.addSubview(shareButton)
        contentView.addSubview(favoriteButton)
        
        let views = ["nameLabel": nameLabel, "quoteLabel": quoteLabel, "shareButton": shareButton, "favoriteButton": favoriteButton] as [String : Any]
        
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "|-[nameLabel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:[nameLabel]-16-[quoteLabel]-40-[favoriteButton]", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        constraints.append(NSLayoutConstraint(item: nameLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 0.3, constant: 0.0))

        constraints.append(shareButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20.0))
        constraints.append(shareButton.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension Selector {
    static let addToFavorites = #selector(AddFavorite.addToFavorites)
}

@objc protocol AddFavorite {
    @objc func addToFavorites(sender: AnyObject?)
}
