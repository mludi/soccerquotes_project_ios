//
//  Quote.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 11/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import Foundation


struct Quote {
    let id: Int
    let name: String
    let body: String
    let createdAt: String
    
    
    init?(dict: [String : AnyObject]) {
        guard let theId = dict["id"] as? Int else { return nil }
        guard let theName = dict["name"] as? String else { return nil }
        guard let theBody = dict["body"] as? String else { return nil }
        guard let theDate = dict["created_at"] as? String else { return nil }
        id = theId
        name = theName
        body = theBody
        createdAt = theDate
    }
}

extension Quote: Equatable {}

func ==(lhs: Quote, rhs: Quote) -> Bool {
    return lhs.id == rhs.id
}