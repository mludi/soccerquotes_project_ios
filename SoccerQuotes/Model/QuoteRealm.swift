//
//  QuoteRealm.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 15.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import RealmSwift
import Foundation

class QuoteRealm: Object {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var body = ""
    dynamic var createdAt = ""
    
}

