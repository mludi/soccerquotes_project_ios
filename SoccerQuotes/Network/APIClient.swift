//
//  QuoteAPI.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 11/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit

struct APICLient {
    
    typealias QuotesCompletionHandler = (_ quotes: [Quote], _ totalPages: Int, _ totalQuotes: Int, _ error: Error?) -> ()
    typealias RegisterTokenCompletionHandler = () -> ()
    typealias SubmitQuoteCompletionHandler = (_ error: Error?) -> ()
    
    static func fetchQuotes(currentPage: Int, completionHandler: @escaping QuotesCompletionHandler) {
        if currentPage == 0 { return }
        var returnList = [Quote]()
        
        guard let theURL = APIEndpoint.Quotes(currentPage).url() else { fatalError() }
        
        let theRequest = URLRequest(url: theURL)
        let theTask = URLSession.shared.dataTask(with: theRequest, completionHandler: { data, response, error in
            if let error = error {
                print(error)
                completionHandler(returnList, 0, 0, error)
                return
            }
            
            if let theResponse = response as? HTTPURLResponse {
                if theResponse.statusCode == 200 {
                    guard let data = data else {
                        completionHandler(returnList, 0, 0, NSError(domain: "Invalid response", code: 222, userInfo: nil))
                        return
                    }
                    
                    do {
                        if let theJSON = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] {
                            guard let theQuotes = theJSON["quotes"] as? [[String: AnyObject]] else {
                                completionHandler(returnList, 0, 0, NSError(domain: "Invalid response", code: 222, userInfo: nil))
                                return
                            }
                            
                            guard let meta = theJSON["meta"] as? [String: AnyObject] else { fatalError() }
                            guard let totalPages = meta["total_pages"] as? Int else { fatalError() }
                            guard let totalQuotes = meta["total"] as? Int else { fatalError() }
                            
                            for theQuote in theQuotes {
                                guard let aQuote = Quote(dict: theQuote) else { continue }
                                returnList.append(aQuote)
                            }
                            completionHandler(returnList, totalPages, totalQuotes, nil)
                        }
                    }
                    catch {
                        print(error)
                        completionHandler(returnList, 0, 0, error)
                    }
                }
            }
            else {
                completionHandler(returnList, 0, 0, NSError(domain: "Invalid response", code: 222, userInfo: nil))
            }
        })
        theTask.resume()
    }
    
    
    static func registerToken(_ inToken: String, debug inDebug: Bool, completionHandler: @escaping RegisterTokenCompletionHandler) {
        let bodyObject = [
            "token": inToken,
            "os": "iOS",
            "debug": inDebug
        ] as [String: Any]
        
        do {
            let theJSON = try JSONSerialization.data(withJSONObject: bodyObject, options: [])
            
            guard let theURL = APIEndpoint.Token.url() else { fatalError() }
            
            var theRequest = URLRequest(url: theURL)
            theRequest.httpMethod = "POST"
            theRequest.httpBody = theJSON
            
            let theTask = URLSession.shared.dataTask(with: theRequest, completionHandler: { data, response, error in
                if let theError = error {
                    print(theError)
                    return
                }
                
                if let theResponse = response as? HTTPURLResponse {
                    if theResponse.statusCode == 200 {
                        completionHandler()
                        return
                    }
                }
            })
            
            theTask.resume()
        }
        catch {
            print(error)
        }
        
    }
    
    static func submitQuote(_ inName: String, body inBody: String, completionHandler: @escaping SubmitQuoteCompletionHandler) {
        let bodyObject = [
            "author": inName,
            "quote": inBody
        ]

        do {
            let theJSON = try JSONSerialization.data(withJSONObject: bodyObject, options: [])
            guard let theURL = APIEndpoint.SubmitQuote.url() else { fatalError() }

            var theRequest = URLRequest(url: theURL)
            theRequest.httpMethod = "POST"
            theRequest.httpBody = theJSON
            let theTask = URLSession.shared.dataTask(with: theRequest, completionHandler: { data, response, error in
                if let theError = error {
                    print(theError)
                    completionHandler(error)
                    return
                }
                
                if let theResponse = response as? HTTPURLResponse {
                    if theResponse.statusCode == 200 {
                        completionHandler(nil)
                        return
                    }
                    else {
                        completionHandler(NSError(domain: "com.sudden-dev.soccerquotes", code: 111, userInfo: nil))
                    }
                }
            })
            theTask.resume()

        }
        catch {
            print(error)
            completionHandler(error)
        }


    }
    
}
