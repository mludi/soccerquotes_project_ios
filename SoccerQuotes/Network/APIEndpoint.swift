//
//  API.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 11/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import Foundation

//private let baseURLString = "http://localhost:3000/api/v1"

private let baseURLString = "https://soccerquotes.sudden-dev.com/api/v1"
//private let baseURLString = "http://soccerquotes/api/v1"
enum APIEndpoint {
    
    case Quotes(Int)
    case Token
    case SubmitQuote
    
    func url() -> URL? {
        switch self {
        case .Quotes(let currentPage):
            return URL(string: "\(baseURLString)/quotes.php?page=\(currentPage)")
        case .Token:
            return URL(string: "\(baseURLString)/devices")
        case.SubmitQuote:
            return URL(string: "\(baseURLString)/quotes.php")
        }
        
    }
    
}

