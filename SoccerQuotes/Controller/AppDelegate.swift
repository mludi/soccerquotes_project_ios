//
//  AppDelegate.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 10/05/16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

    func applicationDidFinishLaunching(_ application: UIApplication) {
        let quotesViewController = UINavigationController(rootViewController: QuotesViewController())
        quotesViewController.tabBarItem.title = NSLocalizedString("Zitate", comment: "Zitate")
        quotesViewController.tabBarItem.setFAIcon(icon: .FAQuoteLeft)
        quotesViewController.isNavigationBarHidden = true
        
        let submitQuoteViewController = UINavigationController(rootViewController: SubmitQuoteViewController())
        submitQuoteViewController.tabBarItem.title = NSLocalizedString("Einreichen", comment: "Einreichen")
        submitQuoteViewController.tabBarItem.setFAIcon(icon: .FAPaperPlane)
        submitQuoteViewController.isNavigationBarHidden = true
        
        let favoritesViewController = UINavigationController(rootViewController: FavoritesTableViewController(style: .plain))
        favoritesViewController.tabBarItem.title = NSLocalizedString("Favoriten", comment: "Favoriten")
        favoritesViewController.tabBarItem.setFAIcon(icon: .FAStar)
        favoritesViewController.isNavigationBarHidden = true
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [quotesViewController, submitQuoteViewController, favoritesViewController]
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
        
        UITabBar.appearance().barTintColor = UIColor(red: 0.906, green: 0.298, blue: 0.235, alpha: 0.50)
        UITabBar.appearance().tintColor = .white
        
        registerForPushNotifications(application)
    }
    
    func registerForPushNotifications(_ inApplication: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
                if !granted {
                    let alertController = UIAlertController(title: NSLocalizedString("Hinweis", comment: ""), message: "Bitte Push-Benachrichtungen aktivieren", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("Einstellungen anzeigen", comment: ""), style: .default) { action in
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString + Bundle.main.bundleIdentifier!) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(settingsAction)
                    alertController.addAction(okAction)
                    self.showErrorAlert(alertController)
                    return
                }
                inApplication.registerForRemoteNotifications()
            }
        }
        else {
            let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            inApplication.registerUserNotificationSettings(notificationSettings)
        }
    }

    func showErrorAlert(_ inAlert: UIAlertController) {
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                self.showErrorAlert(inAlert)
            }
            return
        }
        self.window?.rootViewController?.present(inAlert, animated: true, completion: nil)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        var isDebug: Bool?
        
        #if DEBUG
            isDebug = true
        #else
            isDebug = false
        #endif
        
        guard let debugEnabled = isDebug else { fatalError() }

        APICLient.registerToken(tokenString, debug: debugEnabled) {
            print("created device")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("failed to register \(error)")
    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        if application.applicationState == .active {
            guard let tabBarController = self.window?.rootViewController as? UITabBarController else { return }
            tabBarController.selectedIndex = 0
            guard let viewControllers = tabBarController.viewControllers, let quotesNavigationController = viewControllers[0] as? UINavigationController else { return }
            guard let quotesViewController = quotesNavigationController.topViewController as? QuotesViewController else { return }
            quotesViewController.switchToLatestItem()
        }
        
    }
 
    
}
