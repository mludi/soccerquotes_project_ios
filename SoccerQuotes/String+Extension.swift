//
//  String+Extension.swift
//  SoccerQuotes
//
//  Created by Matthias Ludwig on 14.06.16.
//  Copyright © 2016 Matthias Ludwig. All rights reserved.
//

import Foundation

extension String {
    func replace(inString: String, with: String) -> String {
        return self.replacingOccurrences(of: inString, with: with, options: String.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(inString: " ",  with: "")
    }
    
    func truncate(inLength: Int, inTrailing: String? = "...") -> String {
        if self.characters.count > inLength {
//            return self.substringToIndex(self.startIndex.advancedBy(inLength)) + (inTrailing ?? "")
            return self.substring(to: self.index(self.startIndex, offsetBy: inLength)) + (inTrailing ?? "")
        }
        else {
            return self
        }
    }
    
}
